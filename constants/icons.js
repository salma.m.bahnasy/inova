export const back=require("../assets/icons/001-left-arrow.png");
export const search=require("../assets/icons/002-search.png");
export const close=require("../assets/icons/001-close.png");
export const burgerbar=require("../assets/icons/003-burger-bar.png");
export const location=require("../assets/icons/001-pin.png");

export default {
    back,
    search,
    close,
    burgerbar,
    location


}