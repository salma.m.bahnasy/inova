/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 */

import React from 'react';
import {
  BackHandler,
  Image,
  Pressable,
  StyleSheet,
  Text,
  View,
} from 'react-native';
import { COLORS, FONTS, SIZES } from '../../constants';


function MainHeader(props){


  return (
    <View style={{...styles?.row}}>
      <Pressable
      onPress={()=>{
        BackHandler.exitApp()
      }}
      style={{
        padding:SIZES.base,
        backgroundColor:COLORS?.black,
        justifyContent:'center',
        borderRadius:SIZES.base,
        margin:SIZES.padding,
      }}
      >
        <Image
        source={props?.righticon}
        style={{
          width:18,
          height:18,
          marginHorizontal:SIZES.base,
          tintColor:COLORS?.white,
          ...props?.imageStyle
        }}
        />
      </Pressable>
      <Text style={{...FONTS.h2,fontWeight:'bold',color:COLORS?.black}}>{props?.title}</Text>
    </View>
  );
}

const styles = StyleSheet.create({
 row:{
  flexDirection:'row',
  alignItems:'center',
  marginTop:24
 }
});

export default MainHeader;
