/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 */

import React from 'react';
import {
  Image,
  Pressable,
  StyleSheet,
  Text,
  TextInput,
  View,
} from 'react-native';
import { COLORS, FONTS, SIZES } from '../../constants';


function SearchBar(props) {


  return (
    <View style={{ ...styles?.row, ...styles?.container }}>

      <Image
        source={props?.lefticon}
        style={{
          ...styles?.icon,
          ...props?.imageStyle
        }}
      />
      <TextInput
        onChangeText={(val) => {
          props?.onChangeText(val)
        }}
        style={{
          ...styles?.Text
        }}
        value={props?.val}
        placeholder={'Search'}
        placeholderTextColor={COLORS?.black}
      />
      <Pressable
        onPress={() => {
          props?.clear()
        }}
      >
        <Image
          source={props?.righticon}
          style={{
            ...styles?.icon,
            ...props?.imageStyle
          }}
        />
      </Pressable>

    </View>
  );
}

const styles = StyleSheet.create({
  row: {
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: 24
  },
  container: {
    borderWidth: 1,
    borderColor: COLORS.black,
    width: '90%',
    alignSelf: 'center',
    borderRadius: SIZES.base,
    justifyContent: 'space-between'
  },
  Text: {
    ...FONTS?.body3,
    color: COLORS?.black,
    flex: 1
  },
  icon: {
    width: 18,
    height: 18,
    marginHorizontal: SIZES.base,
    tintColor: COLORS?.black,
  },

});

export default SearchBar;
