import { BaseUrl, token } from "../../../constants/constVariable"

const GetMainData = function (limit) {
    return new Promise((resolve, reject) => {
        let location = 'san jos'
        let uri = `${BaseUrl}businesses/search?location=${location}e&limit=${limit}`
        console.log({ uri })
        fetch(uri, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                'authorization': `Bearer ${token}`
            },
        }).then((response) => response.json())
            .then((responseJson) => {
                console.log("res", responseJson)
                resolve(responseJson?.businesses)

            }).catch((error) => {
                console.error(error);
                let errorObj = {
                    errorReson: error,
                    type: "error"
                }
                resolve(errorObj)
            });
    })
}


export {
    GetMainData
}