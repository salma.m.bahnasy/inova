
import React, { useEffect, useState } from 'react';
import {
  ActivityIndicator,
  FlatList,
  SafeAreaView,
  ScrollView,
  StatusBar,
  useColorScheme,
  View,
  Text,
  StyleSheet,
  ImageBackground,
  Image,
  Pressable
} from 'react-native';

import {
  Colors,
} from 'react-native/Libraries/NewAppScreen';
import MainHeader from '../Components/MainHeader';
import { COLORS, FONTS, icons, SIZES } from '../../constants';
import SearchBar from '../Components/SearchBar';
import { GetMainData } from './Services/services';
import { useNavigation } from '@react-navigation/native';


function Home() {
  const isDarkMode = useColorScheme() === 'dark';
  const backgroundStyle = {
    backgroundColor: isDarkMode ? Colors.darker : Colors.lighter,
  };
  // state------
  let limit = 50
  const navigation = useNavigation()
  const [horizontal, setHorizontal] = useState(false)
  const [mainData, setMainData] = useState([])
  const [AllmainData, setAllMainData] = useState([])
  const [Searcharray, setSearchArr] = useState([])
  const [searchtxt, setSearchtxt] = useState('')

  const [isloading, setIsloading] = useState(true)

  useEffect(() => {
    getMaindata()
  }, [isloading])

  const getMaindata = () => {
    GetMainData(limit).then(response => {
      console.log({ response })
      setAllMainData(response)
      const CostEffective = response.filter(obj => {
        return obj.price === "$";
      });
      const BitPricer = response.filter(obj => {
        return obj.price === "$$";
      });
      const BiSpender = response.filter(obj => {
        return obj.price === "$$$";
      });
      console.log({ CostEffective })
      console.log({ BitPricer })
      console.log({ BiSpender })
      setMainData([
        {
          title: 'Cost Effective',
          data: CostEffective
        },
        {
          title: 'Bit Pricer',
          data: BitPricer
        },
        {
          title: 'Bi Spender',
          data: BiSpender
        }
      ])
      setIsloading(false)

    })
  }
  const searchOnList = (val) => {
    const searchArr = AllmainData.filter((item) => {
      if (item.name.toUpperCase().includes(val.toUpperCase())) {
        return item
      }

    })
    console.log("search", searchArr)
    setSearchArr(searchArr)
  }

  // ........................view--------------------------------
  function renderListView() {


    const renderItem = ({ item, index }) => {
      return <View style={{ padding: SIZES.padding }}>
        <Text
          style={{
            ...styles?.headertitle
          }}
        >{item?.title}</Text>
        <FlatList
          data={item.data}
          numColumns={horizontal ? 2 : 1}
          renderItem={(i, idx) => {
            return (
              <Pressable onPress={() => {
                navigation.navigate('Details', {
                  data: i.item
                })
              }}>
                <ImageBackground
                  source={{ uri: i.item?.image_url }}
                  borderRadius={SIZES?.radius}
                  style={{
                    width: horizontal ? SIZES.width / 2 - 60 : SIZES.width - 40,
                    height: horizontal ? SIZES.width / 2 - 60 : SIZES.width / 3,
                    ...styles?.bgimage

                  }}>
                  <View style={{
                    ...styles.overlay
                  }}>
                    <Text
                      numberOfLines={2}
                      style={{
                        ...FONTS.h2,
                        color: COLORS?.white,
                        paddingTop: 20,
                        fontWeight: 'bold'
                      }}>{i?.item?.name}</Text>

                    <Text
                      numberOfLines={1}
                      style={{
                        ...styles?.smalltxt,
                        width: horizontal ? 'auto' : 180,

                      }}>
                      {`${i?.item?.rating} start  ${i?.item?.review_count} review`}
                    </Text>

                  </View>
                </ImageBackground>
              </Pressable>
            )
          }}
        />
      </View>
    }
    return <FlatList
      data={mainData}
      extraData={mainData}
      renderItem={renderItem}
    />



  }
  function searchListView() {


    const renderItem = ({ item, index }) => {
        return (
        <Pressable onPress={() => {
          navigation.navigate('Details', {
            data:item
          })
        }}>
          <ImageBackground
            source={{ uri: item?.image_url }}
            borderRadius={SIZES?.radius}
            style={{
              width: horizontal ? SIZES.width / 2 - 60 : SIZES.width - 40,
              height: horizontal ? SIZES.width / 2 - 60 : SIZES.width / 3,
              ...styles?.bgimage

            }}>
            <View style={{
              ...styles.overlay
            }}>
              <Text
                numberOfLines={2}
                style={{
                  ...FONTS.h2,
                  color: COLORS?.white,
                  paddingTop: 20,
                  fontWeight: 'bold'
                }}>{item?.name}</Text>

              <Text
                numberOfLines={1}
                style={{
                  ...styles?.smalltxt,
                  width: horizontal ? 'auto' : 180,

                }}>
                {`${item?.rating} start  ${item?.review_count} review`}
              </Text>

            </View>
          </ImageBackground>
        </Pressable>
      )
    }
    return <FlatList
      data={Searcharray}
      extraData={Searcharray}
      renderItem={renderItem}
    />



  }
  return (
    <SafeAreaView >
      <StatusBar
        barStyle={isDarkMode ? 'light-content' : 'dark-content'}
        backgroundColor={backgroundStyle.backgroundColor}
      />
      <ScrollView
      >
        <MainHeader
          righticon={icons?.back}
          title={'Restaurants'}
        />
        <SearchBar
          onChangeText={(val) => {
            setSearchtxt(val)
            searchOnList(val)
          }}
          val={searchtxt}
          lefticon={icons?.search}
          righticon={icons?.close}
          clear={()=>{
            setSearchtxt('')
          }}
        />
        <Pressable
          onPress={() => {
            setIsloading(true)
            setHorizontal(!horizontal)
          }}
        >
          <Image
            source={icons.burgerbar}
            style={{
              ...styles?.mIcon
            }}
          />
        </Pressable>

        {isloading ?
          <ActivityIndicator />
          :
          searchtxt?.length > 0 ?
            searchListView()
            :
            renderListView()
        }


      </ScrollView>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: StatusBar.currentHeight,
    marginHorizontal: 16,
  },
  item: {
    backgroundColor: '#f9c2ff',
    padding: 20,
    marginVertical: 8,
  },
  header: {
    fontSize: 32,
    backgroundColor: '#fff',
  },
  title: {
    fontSize: 24,
  },
  smalltxt: {
    ...FONTS.body5,
    color: COLORS?.black,
    backgroundColor: COLORS.white,
    padding: SIZES.smallpading,
    borderRadius: SIZES.radius
  },
  mIcon: {
    width: 20,
    height: 20,
    marginHorizontal: SIZES.padding,
    alignSelf: 'flex-end',
    marginTop: SIZES.padding
  },
  overlay: {
    backgroundColor: '#000000a6',
    width: '100%',
    height: '100%',
    borderRadius: SIZES.radius,
    justifyContent: 'space-between',
    padding: SIZES.base
  },
  bgimage: {
    alignSelf: 'center',
    justifyContent: 'center',
    margin: SIZES.base,
    borderRadius: SIZES.radius
  },
  headertitle: {
    ...FONTS?.h1,
    color: COLORS?.black,
    fontWeight: 'bold'
  }
});

export default Home;
