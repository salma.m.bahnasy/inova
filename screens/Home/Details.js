
import React, { useEffect, useState } from 'react';
import {
  StatusBar,
  useColorScheme,
  View,
  Text,
  StyleSheet,
  Image,
} from 'react-native';

import {
  Colors,
} from 'react-native/Libraries/NewAppScreen';
import { COLORS, FONTS, icons, SIZES } from '../../constants';
import { useRoute } from '@react-navigation/native';
import MapView, { Marker, PROVIDER_GOOGLE } from 'react-native-maps'; // remove PROVIDER_GOOGLE import if not using Google Maps

function Details() {
  const isDarkMode = useColorScheme() === 'dark';
  const backgroundStyle = {
    backgroundColor: isDarkMode ? Colors.darker : Colors.lighter,
  };

  const route = useRoute()
  // state------
  const [mainData, setMainData] = useState(route?.params?.data)
  const [isloading, setIsloading] = useState(false)





  // ........................view--------------------------------

  function header() {
    return (
      <View
        style={{
          width: '100%',
          height: SIZES.width / 3,
          backgroundColor: COLORS?.white,
          justifyContent: 'center',
          alignContent: 'center',
          alignItems: 'center',
          zIndex: 100
        }}
      >
        <Text
          style={{ ...FONTS?.h2, color: COLORS.black, fontWeight: 'bold' }}
        >{mainData?.name}</Text>
        <Text
          style={{ ...FONTS?.body4, color: COLORS.black }}
        >{mainData?.location?.display_address}</Text>
      </View>
    )
  }
  return (
    < >
      <StatusBar
        barStyle={isDarkMode ? 'light-content' : 'dark-content'}
        backgroundColor={backgroundStyle.backgroundColor}
      />




      {header()}

      <View style={styles.container}>
        <MapView

          provider={MapView.PROVIDER_GOOGLE}
          style={styles.map}
          onPress={(e) => {
            // Keyboard.dismiss()
            console.log(e)
            let cc = e.nativeEvent.coordinate
            console.log('onpres', e.nativeEvent.coordinate)



          }}
          region={{
            latitude: mainData.coordinates.latitude,
            longitude: mainData.coordinates.longitude,
            latitudeDelta: 0.015,
            longitudeDelta: 0.0121,
          }}
        >
          <Marker
            coordinate={{
              latitude: mainData.coordinates.latitude,
              longitude: mainData.coordinates.longitude,
            }}
            title={'location'}
            draggable={false}
            onDragEnd={(e) => {
              console.log('dragEnd', e.nativeEvent.coordinate)


            }}

          >
            <Image source={icons?.location}
              style={{
                width: 50,
                height: 80,
                resizeMode: 'contain',
                tintColor: COLORS?.primary,
              }}
            />
          </Marker>
        </MapView>

      </View>
      <View style={{
        bottom: 0,
        position: 'absolute',
        backgroundColor: COLORS?.white, width: '100%', padding: SIZES.padding
      }}>
        <Text
          style={{ color: COLORS.black, }}
        >{mainData?.location?.address1}</Text>
        <Text
          style={{ color: COLORS.black, }}
        >{mainData?.location?.address2}</Text>

        <View style={{ flexDirection: 'row', alignItems: 'center' }}>
          <Image
            style={{ width: 18, height: 18, marginHorizontal: SIZES.base }}
            source={icons?.location}
          />
          <Text>
            {mainData?.location?.display_address}
          </Text>
        </View>
      </View>


    </>
  );
}

const styles = StyleSheet.create({
  container: {
    ...StyleSheet.absoluteFillObject,
    height: SIZES.height,
    width: "100%",
    justifyContent: 'flex-end',
    alignItems: 'center',
    // flex:1

  },
  map: {
    ...StyleSheet.absoluteFillObject,

  },

  item: {
    backgroundColor: '#f9c2ff',
    padding: 20,
    marginVertical: 8,
  },
  header: {
    fontSize: 32,
    backgroundColor: '#fff',
  },
  title: {
    fontSize: 24,
  },
});

export default Details;
