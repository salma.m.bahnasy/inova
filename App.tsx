/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 */

import React from 'react';
import './Localization/IMLocalize';
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import Home from './screens/Home/Home';
import Details from './screens/Home/Details';



const Stack = createNativeStackNavigator();

function App(): JSX.Element {


  return (
    <NavigationContainer>
    <Stack.Navigator>
      <Stack.Screen
        options={{ headerShown: false }}
        name="Home"
        component={Home} />
      <Stack.Screen
        options={{ headerShown: false }}
        name="Details"
        component={Details} />
    </Stack.Navigator>
  </NavigationContainer>
  );
}


export default App;
